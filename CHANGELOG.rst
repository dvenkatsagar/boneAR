[Current]
^^^^^^^^^

-  `1b39bca <../../commit/1b39bca>`__ - **(Sagar Dwibhashyam)** Finished
   basic requirements.... still need improvements

0.0.15
^^^^^^

-  `1ab38e1 <../../commit/1ab38e1>`__ - **(Sagar Dwibhashyam)** Able to
   show the 3D model in the browser.

0.0.11
^^^^^^

-  `12fefea <../../commit/12fefea>`__ - **(Sagar Dwibhashyam)** Added
   papaya viewer and also experimenting with some features
-  `97225b1 <../../commit/97225b1>`__ - **(Sagar Dwibhashyam)** Added a
   generate Assets feature
-  `7a1a75e <../../commit/7a1a75e>`__ - **(Sagar Dwibhashyam)** Added
   Bone AR project and basic setup is done. # vpat++

