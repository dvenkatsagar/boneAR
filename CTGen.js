var fs = require('fs');
var path = require('path');
var vtk = require('vtk');
var ndarray = require('ndarray');
ndarray.pack = require('ndarray-pack');
ndarray.unpack = require('ndarray-unpack');
ndarray.pool = require('ndarray-scratch');
ndarray.rotate = require("image-rotate");

var CTGEN = {
  set_directory : function(reader, p){
    reader.SetDirectoryName(p);
    reader.update();
    return reader;
  },
  get_ads : function(reader){
    var image = reader.GetOutput();
    var dims = image.GetDimensions();
    var spacing = reader.GetPixelSpacing();

    var x = this.arange(0.0, (dims[0]+1)*spacing[0], spacing[0]);
    var y = this.arange(0.0, (dims[1]+1)*spacing[1], spacing[1]);
    var z = this.arange(0.0, (dims[2]+1)*spacing[2], spacing[2]);

    return [x,y,z,dims,spacing];
  },
  get_threshold_image : function(image,t){
    var threshold = new vtk.vtkImageThreshold();
    threshold.SetInputData(image);
    threshold.ThresholdByLower(t);
    threshold.ReplaceInOn();
    threshold.SetInValue(0);
    threshold.ReplaceOutOn();
    threshold.SetOutValue(1);
    threshold.Update();

    return threshold;
  },
  get_dmc : function(threshold){
    var dmc = new vtk.vtkDiscreteMarchingCubes();
    dmc.SetInputData(threshold.GetOutput());
    dmc.GenerateValues(1, 1, 1);
    dmc.Update();

    return dmc;
  },
  write : function(dmc,p){
    var writer = new vtk.vtkPLYWriter();
    //var writer = new vtk.vtkXMLPolyDataWriter();
    writer.SetInputData(dmc.GetOutput());
    writer.SetFileTypeToBinary();
    //writer.SetFileTypeToASCII();
    //writer.SetDataModeToBinary();
    //writer.SetDataModeToAscii();
    //writer.SetCompressorTypeToNone();
    writer.SetFileName(p);
    writer.Write();

    return writer;
  },
  vtk_to_array : function(image,dims){
    var arrayData = image.GetPointData( ).GetArray( 0 );
    var tmp = ( new vtk[ arrayData.GetClassName( ) ] ).SafeDownCast( arrayData );
    var volume_buffer = tmp.getBuffer();
    var volume = ndarray(volume_buffer,[dims[2],dims[0],dims[1]]);
    return volume;
  },
  rotate_image : function(ndarr,r){
    var tmp = ndarray.pool.zeros(ndarr.shape, ndarr.dtype);
    var arr = ndarray.rotate(tmp,ndarr, r);
    ndarray.pool.free(tmp);
    return arr;
  },
  arange : function(start,stop,step){
    var x = [];
    for (var i = start; i < stop; i = i + step){
      x.push(i);
    }
    return x;
  },
  run : function(p,t){
    var executed = false;
    try{
      var reader = new vtk.vtkDICOMImageReader();

      console.log("Reading DICOM Directory ...");
      reader = this.set_directory(reader,path.join(p,"images"));

      console.log("Getting ADS Values ...");
      var ads = this.get_ads(reader);

      var x = ads[0];
      var y = ads[1];
      var z = ads[2];
      var dims = ads[3];
      var spacing = ads[4];

      console.log("Generating Threshold Image ...");
      if(t.length > 0){
        var threshold = this.get_threshold_image(reader.GetOutput(),parseInt(t));
      }else{
        var threshold = this.get_threshold_image(reader.GetOutput(),400);
      }

      console.log("Generating 3D model ...");
      var dmc = this.get_dmc(threshold);

      console.log("Decimating 3D model ...");
      var decimate = new vtk.vtkDecimatePro();
      decimate.SetInputData(dmc.GetOutput());
      decimate.SetTargetReduction(0.50);
      decimate.Update();


      console.log("Optimizing 3D Model ...");
      var smoothFilter = new vtk.vtkSmoothPolyDataFilter();
      smoothFilter.SetInputData(decimate.GetOutput());
      smoothFilter.SetNumberOfIterations(15);
      smoothFilter.SetRelaxationFactor(0.1);
      smoothFilter.FeatureEdgeSmoothingOff();
      smoothFilter.BoundarySmoothingOn();
      smoothFilter.Update();

      var centerFilter = new vtk.vtkCenterOfMass();
      centerFilter.SetInputData(smoothFilter.GetOutput());
      centerFilter.SetUseScalarsAsWeights(false);
      centerFilter.Update();
      center = centerFilter.GetCenter();

      var transform = new vtk.vtkTransform();
      transform.Translate(-center[0], -center[1], -center[2]);

      var transformFilter = new vtk.vtkTransformPolyDataFilter();
      transformFilter.SetInputData(smoothFilter.GetOutput());
      transformFilter.SetTransform(transform);
      transformFilter.Update();

      console.log("Writing file ...");
      var writer = this.write(transformFilter,path.join(p,"model.ply"));
      console.log("All done");
      var expected = path.join("models",p.split("/")[1],"model.ply");
    }catch (err){
      console.log(err);
      var expected = err;
    }finally{
      return expected;
    }
  }
};

//CTGEN.run("ct_sets/Head",400);
module.exports = CTGEN;
