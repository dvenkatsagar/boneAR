'use strict'
// import modules
var path           = require( 'path' );
var http           = require( 'http' );
var express        = require( 'express' );
var cookieParser   = require( 'cookie-parser' );
var bodyParser     = require( 'body-parser' );
var methodOverride = require( 'method-override' );
var logger         = require( 'morgan' );
var errorHandler   = require( 'errorhandler' );
var serve_static   = require( 'serve-static' );
var favicon        = require( 'serve-favicon' );
var compression    = require( 'compression' );
var session        = require( 'express-session' );
var utils          = require( './utils' );
var mongoose       = require( 'mongoose' );
var modals         = require( "./modals/modals" );
mongoose.connect( 'mongodb://127.0.0.1/express-ctset' );

var CTGEN = require('./CTGen');

// Initialize App
var app = express();

// Set Properties
app.set( 'port', process.env.PORT || 3000 );
app.set('view engine', 'jade');
app.set( 'views', 'views');

// Use Modules
app.use( logger( 'dev' ));
app.use( methodOverride());
app.use( cookieParser());
app.use( bodyParser.json());
app.use( bodyParser.urlencoded({ extended : true }));
app.use(compression());
app.use( serve_static( 'public' ));
app.use( favicon(path.join("public","images","favicon.png")));

// development only
if( 'development' == app.get( 'env' )){
  app.use( errorHandler() );
}

// For multiple users
app.use(session({secret : "AR done awesome", resave:false, saveUninitialized: false}));

// Set Routes using Router
var index = require("./routes/index");
var gen_model = require("./routes/gen-model");
var get_set = require("./routes/get-set");
var show_3D = require("./routes/show-3D");
var show_AR = require("./routes/show-AR");
app.use("/",index);
app.use("/genModel",gen_model);
app.use("/getSet",get_set);
app.use("/show3D",show_3D);
app.use("/showAR",show_AR);

// Create Server from App
http.createServer( app ).listen( app.get( 'port' ), function (){
  console.log( 'Express server listening on port ' + app.get( 'port' ));
});
