var default_params = [];
default_params["kioskMode"] = false;
default_params["showControlBar"] = true;
default_params["fullScreenPadding"] = false;
$(document).ready(function(){
  //var f = encodeURIComponent("models/Head/model.ply");
  //window.open("http://localhost:3000/show3D?file="+f,"_blank");

  var current_model_value = 400;
  var generated_model_once = false;
  var current_model_data = null;
  $("#ct-set-select").on("change",function(e){
    // reset settings for the new set
    current_model_value = 400;
    generated_model_once = false;
    current_model_data = null;
    $("#ct-threshold-input").val(400);

    var ct_set_select = $("#ct-set-select").find("option:selected").attr("value");
    if (ct_set_select == null){
      $("#status").html("");
      var params = [];
      params["kioskMode"] = false;
      params["showControlBar"] = true;
      params["fullScreenPadding"] = false;
      papaya.Container.resetViewer(0,params);
      papaya.Container.resizePapaya();
      $("#ct-threshold-input, .ct-form-buttons a").attr("disabled","disabled");
    }else{
      $("#status").html("<i class='fa fa-spinner fa-pulse'></i> Getting Image Set ...");
      $.get("/getSet",{"set_id" : ct_set_select},function(data){
        $("#status").html("<i style='color:green;' class='fa fa-check-square'></i> Got the Image Set");
        var params = [];
        params["images"] = [data];
        params["kioskMode"] = true;
        params["showControlBar"] = true;
        params["fullScreenPadding"] = false;
        papaya.Container.resetViewer(0,params);
        papaya.Container.resizePapaya();
        $("#ct-threshold-input, #gen-model").removeAttr("disabled");
      });
    }
  });

  $("#ct-threshold-input").on("input",function(){
    var ct_threshold_input =  $("#ct-threshold-input").val();
    if(generated_model_once == true){
      if(current_model_value == ct_threshold_input){
        $(".ct-form-buttons a").removeAttr("disabled");
        $("#gen-model").attr("disabled","disabled");
      }else{
        $(".ct-form-buttons a").attr("disabled","disabled");
        $("#gen-model").removeAttr("disabled");
      }
    }
  });

  $(".ct-form-buttons a").on('click',function(e){
    var ct_set_select = $("#ct-set-select").find("option:selected").attr("value");
    var ct_threshold_input =  $("#ct-threshold-input").val();

    switch($(e.target).attr("id")){
      case "gen-model":
        generated_model_once = true;
        current_model_value = ct_threshold_input;
        $("#status").html("<i class='fa fa-spinner fa-pulse'></i> Generating Model ...");
        $.get("/genModel",{"set_id" : ct_set_select, "threshold" : ct_threshold_input},function(data){
          current_model_data = data;
          $("#status").html("<i style='color:green;' class='fa fa-check-square'></i> Model has been generated");
          $(".ct-form-buttons a").removeAttr("disabled");
          $("#gen-model").attr("disabled","disabled");
        });
        break;
      case "show-3D":
        var f = encodeURIComponent(current_model_data);
        window.open("http://localhost:3000/show3D?file="+f,"_blank");
        break;
      case "show-ar":
        var f = encodeURIComponent(current_model_data);
        window.open("http://localhost:3000/showAR?file="+f,"_blank");
        break;
    }
  });
});
