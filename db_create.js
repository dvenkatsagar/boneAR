var mongoose = require( 'mongoose' );
var modals = require("./modals/modals");
mongoose.connect( 'mongodb://127.0.0.1/express-ctset' );
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("hey connected");
  //create_data();
  //drop_data();
});
function create_data(){
  new modals.CTSet({
    id :  0,
    name : "Head"
  }).save(function(err,set,count){
    if(err) return console.log(err);
  });
}

function drop_data(){
  modals.CTSet.remove({},function(err){
    console.log("collection removed");
  });
}
