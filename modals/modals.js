var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var CTSetSchema = new Schema({
    id    : {type: Number, unique:true},
    name    : {type: String, unique:true}
});

var CTSet = mongoose.model( 'CTSet', CTSetSchema );

module.exports = {
  CTSet : CTSet
}
