var fs = require('fs');
var path = require('path');
var express = require('express');
var mongoose = require("mongoose");
var modals = require(path.resolve("modals/modals"));
var CTSet = modals.CTSet;

var router = express.Router();

router.get("/",function(req,res,next){
  CTSet.findOne({'id':req.query.set_id},"name",function(err,set){
    if (err) return next(err);
    fs.readdir(path.join("ct_sets",set.name,"images"),function(err, files){
      if (err) return next(err);
      var data = files.sort().map(function(f){
        return path.join("images",set.name,f);
      });
      res.send(data);
    });
  });
});

module.exports = router;
