var path = require('path');
var express = require('express');
var mongoose = require("mongoose");
var modals = require(path.resolve("modals/modals"));
var CTGEN = require(path.resolve("CTGen"));
var CTSet = modals.CTSet;

var router = express.Router();

router.get("/",function(req,res,next){
  console.log("Getting CT Set ...");
  CTSet.findOne({'id':req.query.set_id},"name",function(err,set){
    if (err) return next(err);
    var folder = path.join("ct_sets",set.name);
    var ret = CTGEN.run(folder,req.query.threshold);
    if (ret){
      res.send(ret);
    }else{
      next(ret);
    }
  });
});

module.exports = router;
