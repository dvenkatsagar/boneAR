var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var modals = require(path.resolve("modals/modals"));
var CTSet = modals.CTSet;

var router = express.Router();

router.get("/",function(req,res,next){
  CTSet.find(function(err,sets){
    if(err) next(err);
    res.render('index',{"sets":sets});
  });
});

router.get("/images/:set/:img",function(req,res){
  res.sendFile(path.join(req.params.set,"images",req.params.img),{root : "ct_sets"});
});

router.get("/models/:set/:model",function(req,res){
  res.sendFile(path.join(req.params.set,req.params.model),{root : "ct_sets"});
});

module.exports = router;
