var express = require('express');
var mongoose = require('mongoose');
var path = require('path');

var router = express.Router();

router.get("/",function(req,res,next){
  res.render("show-3D",{file : req.query.file});
});

module.exports = router;
